#include <wx/wx.h>

#include "Executer.h"
#include "MainWindow.h"

long Executer::execute(wxTextCtrl *log, bool quiet) {
	wxArrayString out;

	wxString *cmd = new wxString(*(this->getCmd()));

	cmd->Append(args);

	if (!quiet)
	{
		log->AppendText(wxT("Execute: "));
		log->AppendText(*cmd);
		log->AppendText(wxT("\nResult: \n"));
	}

	long rv = wxExecute(*cmd, out, out, wxEXEC_SYNC);

	if (!quiet)
	{
		for (size_t i = 0; i < out.GetCount(); i++) {
			log->AppendText(out[i]);
			log->AppendText(wxT("\n"));
		}

		log->AppendText(wxT("\n"));
	}

	delete cmd;

	return rv;
}

void Executer::setArgs(wxString args) {
	this->args = args;
}
