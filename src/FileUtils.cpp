#include <wx/wx.h>
#include <sys/types.h>
#include <unistd.h>

#include "FileUtils.h"

wxString FileUtils::getTmpFileName()
{
	wxString result;

	result = wxT("/tmp/unpaper-tmp-");
	result += wxString::Format(wxT("%d"), getpid());
	result.Append(wxT("-"));
	result += wxString::Format(wxT("%d"), rand());

	return result;
}

wxString FileUtils::getTmpFileName(wxString ext)
{
	return getTmpFileName() + ext;
}

wxString FileUtils::getQuotedTmpFileName()
{
	return quoteName(getTmpFileName());
}

wxString FileUtils::getQuotedTmpFileName(wxString ext)
{
	return quoteName(getTmpFileName(ext));
}


wxString FileUtils::quoteName(wxString name)
{
	return wxT("\"") + name + wxT("\"");
}

void FileUtils::rmFile(wxString path)
{
	wxExecute(wxT("rm -f ") + path);
}
