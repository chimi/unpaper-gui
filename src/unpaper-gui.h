#ifndef __WXWIDGETSAPP_H
#define __WXWIDGETSAPP_H

#include <wx/wx.h>

class wxUnpaperGUI : public wxApp
{
public:
    wxUnpaperGUI();
    virtual ~wxUnpaperGUI();
    virtual bool OnInit();
};

DECLARE_APP(wxUnpaperGUI)

#endif 
