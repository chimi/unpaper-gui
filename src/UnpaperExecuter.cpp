#include "UnpaperExecuter.h"
#include "ConvertExecuter.h"
#include "FileUtils.h"
#include "Options.h"

UnpaperExecuter::UnpaperExecuter()
{
	cmd = wxT("unpaper ");
}

wxString *UnpaperExecuter::getCmd()
{
	return &(this->cmd);
}

long UnpaperExecuter::unpaper(wxString from, wxString to, Options *opts, wxTextCtrl *log) // names unquoted
{
	wxString *args = new wxString();
	wxString convertedFrom = wxT("");

	if (from.EndsWith(wxT("pnm")) || from.EndsWith(wxT("pbm")) || from.EndsWith(wxT("pgm")) || from.EndsWith(wxT("ppm")))
		args->Append(FileUtils::quoteName(from));
	else {
		// conversion needed
		ConvertExecuter *converter = new ConvertExecuter();
		convertedFrom = FileUtils::getTmpFileName(wxT(".pnm"));

		int rv = converter->convert(from, convertedFrom, log); // watch this NULL

		delete converter;

		if (rv)
			return rv;

		args->Append(FileUtils::quoteName(convertedFrom));
	}

	args->Append(wxT(" "));
	args->Append(FileUtils::quoteName(to));
	//args->Append(wxT(" "));
	args->Append(opts->render());

	//wxMessageBox(*args, wxT("unpap-exec"));

	this->setArgs(*args);

	long rv = this->execute(log);

	delete args;

	if (convertedFrom.Length() > 0)
		FileUtils::rmFile(convertedFrom);

	return rv;
}

